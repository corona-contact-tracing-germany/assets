![CCTG header](https://codeberg.org/corona-contact-tracing-germany/assets/raw/branch/main/handholdingphone/results/header.png)

In this repository, you can find graphic files that are created in the context of Corona Contact Tracing Germany.

Assets may have different licenses. Please check the `README` and `LICENSE` files in each directory.

#### Colors

These are the colors we are using for the gradient in our app icon:
* blue (`#56b4ea`)
* green (`#98ea56`)

For the gradients in the app, we are using these less bright versions:
* blue (`#2f9bd8`)
* green (`#73d326`)
